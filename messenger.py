class Messenger:
    """
    All messages in console are printed here. control verbose here
    """

    def __init__(self, players):
        self.players = players
        self.p1 = players[0]
        self.p2 = players[1]
        if self.p1.human:
            self.human_player = self.p1
        elif self.p2.human:
            self.human_player = self.p2
        else:
            self.human_player = False

    def game_starting_message(self):
        print("Game start! Good luck!")

    def round_start_message(self, blinds, sb_i):
        print("===================================================================")
        print("Blind amount: {} / {}" .format(blinds[0], blinds[1]))
        print("SB: {}, stack size: {}" .format(self.players[sb_i], self.players[sb_i].stack))
        print("BB: {}, stack size: {}" .format(self.players[1-sb_i], self.players[1-sb_i].stack))
        print("{} posts 0.5 BB ({}); {} posts 1 BB ({})" .format(self.players[sb_i], blinds[0], self.players[1-sb_i], blinds[1]))

    def street_start_message(self, street, pot):
        print("++++++++++++++++++++++++++++++")
        print(self._street_name(street), "(Pot: {})" .format(pot))
        print("-----------")

    def board(self, board):
        print("Board: {}" .format(board))
    
    def show_down(self, board):
        print("{} hole cards: {}" .format(self.p1, self.p1.cards))
        print("{} hole cards: {}" .format(self.p2, self.p2.cards))
    
    def pot_distributor(self, pot, winner):
        if len(winner) == 2:
            print("It's a tie, {} is split between two players" .format(pot))
        else:
            print("{} is the winner" .format(winner[0]))
            print("{} is distrubuted to {}" .format(pot, winner[0]))

    def announce_action(self, player, action, pot):
        print("{} {}, amount: {}. Stack: {}. pot: {}" .format(player, action["action"], action["amount"], player.stack, pot))
        if self.human_player != False:
            input("Press Enter to start next action...")

    def look_hole(self):
        """
        Show human player his/her hole cards
        """
        if not self.human_player:
            pass
        else:
            print("Your hole cards: {}" .format(self.human_player.cards))

    def next_round_ask(self):
        if self.human_player != False:
            input("Press Enter to start next round...")

    def final_message(self, winner):
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("{} wins the game! Congrats!" .format(winner))

    def _street_name(self,street):
        street_name = {
            0: "Blinds",
            1: "Preflop",
            2: "Flop",
            3: "Turn",
            4: "River"
        }
        return street_name[street]