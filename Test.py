

from dealer import *

p1 = AI.Call_man("Jack", 112)
p2 = AI.Human("Rose", 107, human=True)
players = [p1, p2]
game_structure = {
    "blinds" : [15,30],
    "start_stack" : 500,
    "max_round": 60,
    "speed" : "regular"
}

game = Dealer(players, game_structure)
game.headsup()

"""
from Deck_Cards import *
from Hand_Evaluate import *

deck = StandardDeck(showing=True)
deck.shuffle()

board = [deck.deal(),deck.deal(),deck.deal(),deck.deal(),deck.deal()]
p1 = Player("Jack", 100)
p2 = Player("Rose", 200)
for i in range(2):
    p1.addCard(deck.deal())
    p2.addCard(deck.deal())

Eva = Hand_Scorer()
print(board)
print(p1.cards)
print(p2.cards)
winner = Eva.winner([p1, p2], board)
"""
