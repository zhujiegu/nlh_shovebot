from itertools import cycle
from messenger import Messenger
#from player import Player

class Action_manager:
    """
    Invoke once in every street
    Give information to players in turn
    Get actions from players
    Implement actions
    """
    def __init__(self, players):
        self.street_status = {

        }
        self.__players = players
        self.gap = 0
        self.min_raise = 0
        self.agreed = False
        self.__messenger = Messenger(players)

    def street_action(self, street, status, sb_i, blinds):
        """
        Super manager of actions in a street
        """
        self.__street_reset(street, blinds[1])
        _n = cycle(range(2))
        # Preflop
        if street == 1: 
            # if bb is all-in or sb will be all-in if call, don't need to ask bb
            if self.__players[1-sb_i].all_in | (self.gap > self.__players[sb_i].stack):
                self.__players[1-sb_i].ask_once = True
            while not self.agreed:
                in_action = sb_i^next(_n) # start with sb
                _action = self.ask_action(street, self.__players, in_action, status)
                self.action_implementor(_action, self.__players, in_action, status)
                self.publish_action(self.__players[in_action], _action, status["pot"])
                self.all_agree(_action)
                
        # Other streets
        else:
            next(_n)
            while not self.agreed:
                in_action = sb_i^next(_n) # start with bb
                _action = self.ask_action(street, self.__players, in_action, status)
                self.action_implementor(_action, self.__players, in_action, status)
                self.publish_action(self.__players[in_action], _action, status["pot"])
                self.all_agree(_action)

    
    def ask_action(self, street, players, in_action, status):
        """
        Generate legitimate action and pass to player
        receive response
        """
        _legal = self.legal_action(street, players, in_action, status)
        _response = players[in_action].response(_legal)
        return _response
        #stakes = [player.stake for player in self.players]
        #pot = sum(stakes)
        #gap = abs(stakes[0] - stakes[1])

    def legal_action(self, street, players, in_action, status):
        """
        call amount: how much more needed = gap
        raise amount: raise to ..
        """
        player = players[in_action]
        _other = players[1-in_action]

        min_raise = _other.bet + self.min_raise
        max_raise = player.bet + player.stack
        if max_raise < min_raise:
            min_raise = max_raise

        if self.gap == 0:
            if street == 1: # BB can raise when SB calls
                return [
                    { "action" : "check" , "amount" : 0 },
                    { "action" : "raise" , "amount" : { "min": min_raise, "max": max_raise } }
                ]
            else:
                return [
                    { "action" : "check" , "amount" : 0 },
                    { "action" : "bet" , "amount" : { "min": min_raise, "max": max_raise } }
                ]
        elif _other.all_in == True:
            return [
                { "action" : "fold" , "amount" : 0 },
                { "action" : "call" , "amount" : min(self.gap, player.stack)}
            ]
        elif self.gap <= player.stack:
            return [
                { "action" : "fold" , "amount" : 0 },
                { "action" : "call" , "amount" : self.gap },
                { "action" : "raise", "amount" : { "min": min_raise, "max": max_raise } }
            ]
        else: # self.gap > player.stack
            return [
                { "action" : "fold" , "amount" : 0 },
                { "action" : "call" , "amount" : player.stack },
            ]
        
    def action_implementor(self, action, players, in_action, status):
        """
        set player.bet, .stack, .all_in, .fold
        set pot, side_pot and update gap
        """
        # player in action and the other player
        player = players[in_action]
        _other = players[1-in_action]
        player.ask_once = True

        if action["action"] == "check":
            pass # no action needed
        
        if action["action"] == "bet":
            self.gap = action["amount"]
            self.min_raise = max(self.min_raise, self.gap)
            player.bet = action["amount"]
            player.stack -= action["amount"]
            if player.stack == 0:
                player.all_in = True
            if action["amount"] <= _other.stack:
                status["pot"] += action["amount"]
            else:
                status["pot"] += _other.stack
                status["side_pot"] = action["amount"] - _other.stack
                status["side_pot_belong"] = player
        
        if action["action"] == "call":
            self.gap = 0
            player.bet += action["amount"]
            player.stack -= action["amount"]
            status["pot"] += action["amount"]
            if player.stack == 0:
                player.all_in = True

        if action["action"] == "raise":
            _previous = player.bet
            player.bet = action["amount"]
            self.gap = player.bet - _other.bet # this is the effective raise after calling
            self.min_raise = max(self.min_raise, self.gap)
            player.stack -= (player.bet - _previous)
            if player.stack == 0:
                player.all_in = True
            if self.gap <= _other.stack:
                status["pot"] += (player.bet - _previous)
            else:
                status["side_pot"] = self.gap - _other.stack
                status["pot"] += (player.bet - _previous) - status["side_pot"]
                status["side_pot_belong"] = player

        if action["action"] == "fold":
            player.fold = True


    def all_agree(self, action):
        """
        check if players have folded or both agreed on the amount
        """
        # anyone folded, street finishes, TO DO: round finish too
        if any([player.fold for player in self.__players]):
            self.agreed = True
        else:        
            # if have asked both, and the action is "check" or "call", street finishes
            if all([player.ask_once for player in self.__players]):
                if action["action"] in ["check", "call"]:
                    self.agreed = True                    

    def publish_action(self, player, action, pot):
        """
        Pass the action from the player in action to all the players for information
        """
        # write info to the other player
        self.__messenger.announce_action(player, action, pot)

    def __street_reset(self, street, bb):
        self.min_raise = bb # BB by default
        self.__players[0].ask_once = self.__players[1].ask_once = False # reset
        self.agreed = False
        if street == 1:
            self.gap = abs(self.__players[0].bet - self.__players[1].bet)
        else:
            self.gap = 0
            self.__players[0].bet = self.__players[1].bet = 0
        # set all-in, fold in dealer