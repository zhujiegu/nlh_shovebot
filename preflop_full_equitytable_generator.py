import subprocess
import deck_cards
import itertools
import multiprocessing
from copy import copy
from joblib import Parallel, delayed
import time

# Takes 9 hours on a 16 CPU machine
# Calculated on Google cloud compute

class EquityEvaluator:

    def __init__(self):
        self.deck = deck_cards.StandardDeck(showing=True)
        self.deck_cards = self.format_to_pokerstove(self.deck.cards, str=False)
        self.full_table = {}

    def single_hand(self, hand1, hand2):
        """
        Get results from Pokerstove
        """
        #hand1 = "AcKs" 
        #hand2 = "7c8d"
        lines = subprocess.run(['/home/z/Python_project/pokerstove/build/bin/ps-eval', hand1, hand2], stdout=subprocess.PIPE).stdout.decode().split('\n')        
        equity = float(lines[0].split()[4])
        return equity

    def one_hand_vs_all_other(self, hand):
        deck = copy(self.deck_cards)
        hand = self.format_to_pokerstove(hand, str=False)
        deck.remove(hand[0])
        deck.remove(hand[1])
        hand = ("".join(hand))
        self.full_table[hand] = {}
        remain_hands = list(itertools.combinations(deck, 2))
        remain_hands = [''.join(i) for i in remain_hands]
        for villain_hand in remain_hands:
            self.full_table[hand][villain_hand] = self.single_hand(hand, villain_hand)

    def format_to_pokerstove(self, cards, str=True):
        stove_format = list()
        for card in cards:
            stove_format.append(card.name + card.suit)
        if str:
            stove_format = ("".join(stove_format))
        return stove_format

    def one_hand_vs_all_other_prl(self, hand):
        """
        Parallel on Linux
        """
        deck = copy(self.deck_cards)
        hand = self.format_to_pokerstove(hand, str=False)
        deck.remove(hand[0])
        deck.remove(hand[1])
        hand = ("".join(hand))
        self.full_table[hand] = {}
        remain_hands = list(itertools.combinations(deck, 2))
        remain_hands = [''.join(i) for i in remain_hands]
        # Parallel 
        num_cores = multiprocessing.cpu_count()
        eqi_list = Parallel(n_jobs=num_cores, prefer="threads")(delayed(self.single_hand)(hand,villan_hand) for villan_hand in remain_hands)
        self.full_table[hand].update(dict(zip(remain_hands, eqi_list)))

    def all_vs_all(self, verbose = True):
        all_hands = list(itertools.combinations(self.deck.cards, 2))
        all_hands = [list(i) for i in all_hands]
        total = len(all_hands)
        _i = 1
        for hand in all_hands:
            if verbose:
                t = time.localtime()
                current_time = time.strftime("%H:%M:%S", t)
                print(current_time)
                print("Calculating {}/{}" .format(_i, total))
                _i += 1
            self.one_hand_vs_all_other_prl(hand)
        print("Complete")
            # self.one_hand_vs_all_other(hand)


equity = EquityEvaluator()
equity.all_vs_all()

import pickle

# Save
a_file = open("./Obj/preflop_full_table.pkl", "wb")
pickle.dump(equity.full_table, a_file)
a_file.close()

# Load
# a_file = open("./Obj/preflop_full_table.pkl", "rb")
# output = pickle.load(a_file)
# print(output)
