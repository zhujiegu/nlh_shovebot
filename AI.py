
from player import Player

class Shove_fold (Player):
    """
    Shove or fold GTO
    """
    def __init__(self, eff_stack):
        pass


class Call_man (Player):    
    """
    Calling station
    """
    def response(self, legal):
        return [res for res in legal if res["action"] in ["call", "check"]][0]

class Allin_man (Player):
    """
    All in maniac
    """
    def response(self, legal):
        # Can I raise/bet or can just call?
        _if_raise = [res for res in legal if res["action"] in ["bet", "raise"]]
        if not _if_raise:
            return [res for res in legal if res["action"] == "call"][0]
        else:
            _max_amount = [res for res in legal if res["action"] in ["bet", "raise"]][0]
            _max_amount["amount"] = _max_amount["amount"]["max"]
            return _max_amount

class Human (Player):
    """
    set human = True, interactive
    """
    def response(self, legal):
        _response = {}
        print("Your hole cards: {}" .format(self.cards))
        print("Your legal actions:")
        for act in legal:
            print("{}: amount: {}" .format(act["action"], act["amount"]))
            
        while True:
            _response["action"] = input("Enter your action: ")
            if _response["action"] not in [res["action"] for res in legal]:
                print("action not legal")
            else:
                break

        if _response["action"] in ["check", "fold", "call"]:
            return [res for res in legal if res["action"] == _response["action"]][0]
        else:
            legal_bet_raise = [res for res in legal if res["action"] in ["bet", "raise"]][0]
            while True:
                while True:
                    try:
                        _response["amount"] = int(input("Enter amount: "))
                        break
                    except ValueError:
                        print("please enter an integer")
                
                if (_response["amount"] < legal_bet_raise["amount"]["min"]) | (_response["amount"] > legal_bet_raise["amount"]["max"]):
                    print("amount not legal")
                else:
                    break
            return _response

    def human_action_checker(self, action, legal):
        if action not in legal:
            print("action not legal, please re-enter: ")