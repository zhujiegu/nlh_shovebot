import pandas as pd

class Range_shove:
    """
    Shove range of SB givn effect stack size
    """
    def __init__(self, eff_size):
        self.eq_table = pd.read_pickle('./Obj/preflop_short_table.pkl')
        self.eff_size = eff_size
        self.shove_range = []
        self.call_range = list(self.eq_table.columns)

    def nr_hands(self, hand, range):
        """
        Number of hands in the range after removal
        """
        _list = self.eq_table.loc[hand, range]
        return sum(list(map(lambda x: x[1], _list)))

    def eq_hand_vs_range(self, hand, range):
        _nr = self.nr_hands(hand, range)
        return sum(list(map(lambda x: x[0]*x[1], self.eq_table.loc[hand, range])))/_nr/100

    def prob_fold(self, hand, range):
        """
        fold probability given hero hand and villian calling range
        blocker take into account
        """
        _nr = self.nr_hands(hand, range)
        return 1 - _nr/1325

    def if_shove_hand(self, hand):
        """
        If a single hand should be shoved from SB given villian calling range
        """
        prob_fold = self.prob_fold(hand, self.call_range)
        win_rate_called = self.eq_hand_vs_range(hand, self.call_range)
        EV = prob_fold*1 + (1-prob_fold)*((2*win_rate_called-1)*self.eff_size) + 0.5
        # print(EV)
        return (True if EV >= 0 else False)

    def if_call_hand(self, hand):
        """
        If a single hand should be called from BB given shoving range
        """
        win_rate = self.eq_hand_vs_range(hand, self.shove_range)
        eq_needed = (self.eff_size-1)/(2*self.eff_size)
        return (True if win_rate >= eq_needed else False)

    def get_shove_range(self):
        """
        loop over all the hands and see if the hand should be shoved
        """
        # Reset
        self.shove_range = []
        for hand in list(self.eq_table.index):
            if self.if_shove_hand(hand):
                self.shove_range.append(hand)

    def get_call_range(self):
        """
        loop over all the hands and see if the hand should be called
        """
        # Reset
        self.call_range = []
        for hand in list(self.eq_table.index):
            if self.if_call_hand(hand):
                self.call_range.append(hand)

    def range_iterate(self):
        """
        iterate until the ranges converge
        """
        if_cvg = False
        nr_itr = 0
        while not if_cvg:
            shove_range_old = self.shove_range
            self.get_shove_range()
            call_range_old = self.call_range
            self.get_call_range()
            if_cvg = (shove_range_old == self.shove_range) & (call_range_old == self.call_range)
            nr_itr = nr_itr + 1
            
            print(self.shove_range)
            print(if_cvg)
        print(nr_itr)
