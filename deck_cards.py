"""
Card, Deck and Player
"""

import random

class Card:
    """
    A single card (name: 'A','K'...,  suit: 'h','s',..., showing: Face down or up)
    """
    def __init__(self, name, suit, showing = True):
        suits = {'s':'♠','h':'♥','c':'♣','d':'♦'}
        # Dictionary {name: (value,prime)}
        values_prime = {
            'A':(14,41),
            'K':(13,37),
            'Q':(12,31),
            'J':(11,29),
            'T':(10,23),
            '9':(9,19),
            '8':(8,17),
            '7':(7,13),
            '6':(6,11),
            '5':(5,7),
            '4':(4,5),
            '3':(3,3),
            '2':(2,2)}
        self.name = name
        self.value = values_prime[name][0]  # value: 14,13,...,
        self.prime = values_prime[name][1] 
        self.suit = suit
        self.symbol = name + suits[suit]  # symbol: A♥,...
        self.showing = showing
    # a representative wrapper
    def __repr__(self):
        if self.showing:
            return self.symbol
        else:
            return "Card"


class StandardDeck (list):
    """
    Deck of cards (a list), use True or False to decide face down or up
    .shuffle() shuffles the deck
    .deal() pops one card (in class Card) from the deck.
    """
    def __init__(self, showing = False):
        self.cards = []
        for name in ['A','K','Q','J','T','9','8','7','6','5','4','3','2']:
            for suit in ['s','h','c','d']:
                self.cards.append(Card(name, suit, showing))

    def shuffle(self, Ntimes = 1):
        random.shuffle(self.cards)
        # print('Deck shuffled')

    def deal(self):
        return self.cards.pop(0)

    def __repr__(self):
        return "Standard deck of cards: {0} cards remaining".format(len(self.cards))


# GUI Tkinter


