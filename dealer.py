from deck_cards import *
from hand_evaluate import *
from player import *
from messenger import Messenger
import action
import AI
from itertools import cycle
from math import ceil,floor


class Dealer:
    """
    First version of two players

    Players: 2, order
    blinds
    board
    pot + player.stake

    1. shuffle, deal players
    2. SB, BB stake
    3. Ask Action (def action) until stake_gap = 0
    4. deal board
    ....
    5. Evaluate hand, find winner
    6. assign pot

    """
    def __init__(self, players, game_structure):
        """
        player1 = Player()
        """
        self.deck = StandardDeck(showing=True)
        self.players = players
        self.sb = players[0]
        self.bb = players[1]
        self.blinds = game_structure["blinds"]
        self.any_all_in = False
        self.any_fold = False
        self.action_finish = False
        self.round_status = {
            "current_street": self.__street_name(0),
            "board" : [],
            "pot": 0,
            "side_pot":0,
            "side_pot_belong":"None",
        }
        self.max_round = game_structure["max_round"]
        self.winner = "Good one"
        self.final_finish = False
        self.action_manager = action.Action_manager(players)
        self.evaluator = Hand_Scorer()
        self.messenger = Messenger(players)

    def headsup(self):
        """
        Main game
        """
        self.messenger.game_starting_message()
        _sb = cycle(range(2))
        _n = 0
        while not self.final_finish and _n < self.max_round:
            self.messenger.next_round_ask()
            _sb_i = next(_sb)
            self.__round_info_reset(_sb_i)
            self.one_round(_sb_i)
            # print("n = %d" %_n)
            _n +=1
            if any(player.stack == 0 for player in self.players):
                self.final_finish = True
        self.__final_summary()


    def one_round(self, sb_i): # sb_i is the index of SB
        """
        One round of game
        """
        self.deck.shuffle()
        self.messenger.round_start_message(self.blinds, sb_i)
        
        # Deal hole cards and Take blinds
        street = 0
        self.blind_taker()
        self.deal_hole()
        self.__pre_preflop_update()
        
        # Pre-flop, Flop, Turn, River
        while not self.action_finish:
            street +=1
            self.messenger.street_start_message(street, self.round_status["pot"])
            self.deal_board(street)
            self.action_manager.street_action(street, self.round_status, sb_i, self.blinds)
            self.__update_table() # check if round has finished
            if street == 4:
                self.action_finish = True

        if self.any_fold:
            self.pot_distributor(self.winner)
        else:
            # Deal cards if finished early
            while street < 4:
                street +=1
                self.deal_board(street)
                
            # Who is the winner
            self.messenger.show_down(self.round_status["board"])
            self.winner = self.evaluator.winner(self.players, self.round_status["board"])
            self.pot_distributor(self.winner)

    # rotate the positions
    #def rotate(self, l, n=1):
    #    return l[n:] + l[:n]

    def __round_info_reset(self, i):
        """
        Current SB, BB, player position
        """
        # reset status
        self.deck = StandardDeck(showing=True)
        self.action_finish = False
        self.show_down = True
        self.any_all_in = False
        self.any_fold = False
        self.sb = self.players[i]
        self.bb = self.players[1-i]
        self.sb.reset()
        self.bb.reset()

        self.round_status = {
            "current_street": self.__street_name(0),
            "board" : [],
            "pot": 0,
            "side_pot":0,
            "side_pot_belong":"None",
        }
        # table rotation
        
        # Blinds increase over time

    def __final_summary(self):
        self.messenger.final_message(self.winner[0])

    def blind_taker(self):
        """
        set player.bet and player.street_bet, subtract from player.stack
        """
        # SB:
        self.sb.bet = min(self.sb.stack, self.blinds[0])
        self.sb.stack -= self.sb.bet
        if self.sb.stack == 0:
            self.sb.all_in = True

        # BB:
        self.bb.bet = min(self.bb.stack, self.blinds[1])
        self.bb.stack -= self.bb.bet
        if self.bb.stack == 0:
            self.bb.all_in = True
            

    def pot_distributor(self, winner):
        # if a tie?
        if len(winner) == 2:
            self.sb.stack += floor(self.round_status["pot"]/2) # floor, ceil deal with pot with odd number
            self.bb.stack += ceil(self.round_status["pot"]/2)
        else:
            winner[0].stack += self.round_status["pot"]
        # is there side pot?
        if self.round_status["side_pot"] != 0:
            self.round_status["side_pot_belong"].stack += self.round_status["side_pot"]
        
        self.messenger.pot_distributor(self.round_status["pot"], winner)

    def __pre_preflop_update(self):
        # situations where round finishes
        self._any_all_in()
        if self.any_all_in:
            # situations where round finishes
            if self.sb.all_in: # if SB is all-in, BB may or may not
                self.round_status["side_pot"] = abs(self.bb.bet - self.sb.bet)
                self.round_status["pot"] = 2 * min(self.sb.bet, self.bb.bet)
                self.round_status["side_pot_belong"] = [self.sb, self.bb][int(self.bb.bet - self.sb.bet) > 0]
                self.action_finish = True
            elif self.bb.bet <= self.sb.bet: # SB not all-in, BB all-in with less than SB
                self.round_status["side_pot"] = self.sb.bet - self.bb.bet
                self.round_status["pot"] = 2 * self.bb.bet
                self.round_status["side_pot_belong"] = self.sb
                self.action_finish = True

        # situations where round has not finished
        if not self.action_finish:
            # round not finish, but side pot needed
            if self.bb.bet > self.sb.bet + self.sb.stack:
                self.round_status["side_pot"] = self.bb.bet - (self.sb.bet + self.sb.stack)
                self.round_status["pot"] = self.sb.bet + (self.sb.bet + self.sb.stack)
                self.round_status["side_pot_belong"] = self.bb
            # Normal situation
            else:
                self.round_status["pot"] = self.sb.bet + self.bb.bet


    def __update_table(self):
        self._any_all_in()
        self._any_fold()
        if self.any_all_in | self.any_fold :
            self.action_finish = True

    def deal_hole(self):
        for i in range(2):
            self.sb.cards.append(self.deck.deal())
            self.bb.cards.append(self.deck.deal())            
        self.messenger.look_hole()

    def deal_board(self,street):
        if street == 1:
            pass
        elif street == 2: # Flop
            self.round_status["board"] = [self.deck.deal(),self.deck.deal(),self.deck.deal()]
            self.messenger.board(self.round_status["board"])
        else: # Turn, River
            self.round_status["board"].append(self.deck.deal())
            self.messenger.board(self.round_status["board"])

    def _any_all_in(self):
        if any([player.all_in for player in self.players]):
            self.any_all_in = True

    def _any_fold(self):
        if any([player.fold for player in self.players]):
            self.action_finish = True
            self.any_fold = True
            self.winner = [player for player in self.players if not player.fold]

    def __street_name(self,street):
        _street_name = {
            0: "Blinds",
            1: "Preflop",
            2: "Flop",
            3: "Turn",
            4: "River"
        }
        return _street_name[street]