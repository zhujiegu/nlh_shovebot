import pickle
import pandas as pd
#from preflop_short_equitytable_generator import Preflop_visualize

a_file = open("./Obj/preflop_full_table.pkl", "rb")
full_table = pickle.load(a_file)
 
# full_short_mapping
all_combo = list(full_table.keys())

def full_name_to_short(handstr):
    if handstr[0]==handstr[2]:
        return handstr[0]+handstr[2]
    elif handstr[1]==handstr[3]:
        return handstr[0]+handstr[2]+'s'
    else:
        return handstr[0]+handstr[2]+'o'

all_combo_shortname = [full_name_to_short(i) for i in all_combo]
full_short = pd.DataFrame(zip(all_combo, all_combo_shortname), columns = ['full_nm','short_nm'])

# convert the full_table from dict to dataframe
full_table_matrix = pd.DataFrame.from_dict(full_table, orient="index")
full_table_matrix.iloc[1:10,1:10]

full_table_matrix.iloc[1,].isnull().sum()  # Check the number of NaN
full_table_matrix.iloc[1,].notnull().sum()  # Check the number of NaN


pd.set_option('display.max_rows', 20, 'display.max_columns', 20)  # Display options

full_table_matrix.mean(axis = 1, skipna = True) # rowmeans

# Check a small part
full_table_matrix.loc[full_table_matrix.index.str.contains(r'A(.)6\1'), full_table_matrix.columns.str.contains(r'K(.)4(?!\1)')]

def equity_shortnm(hero_hd, villan_hd):
    """
    Calculate the equity of short name vs short name by averaging all the hands included
    """
    # full names of possible hands based on the short name
    hero_hds = full_short[full_short['short_nm'] == hero_hd]['full_nm'].tolist()
    villan_hds = full_short[full_short['short_nm'] == villan_hd]['full_nm'].tolist()
    _matrix = full_table_matrix.loc[hero_hds, villan_hds]
    # return the mean
    _mean = _matrix.mean(axis = 1, skipna = True).mean()
    _number_hds = _matrix.iloc[1,].notnull().sum()
    return (_mean, _number_hds)


# list of unique hands
short_combo = list(set(full_short['short_nm'].tolist()))

# create short_table
short_table_matrix = pd.DataFrame(index = short_combo, columns = short_combo)
short_table_matrix.loc['AA','22']

for hero_hd in short_combo:
    for villan_hd in short_combo:
        short_table_matrix.loc[hero_hd,villan_hd] = equity_shortnm(hero_hd,villan_hd)

short_table_matrix

short_table_matrix.to_pickle('./Obj/preflop_short_table.pkl') 
# df = pd.read_pickle('./Obj/preflop_short_table.pkl')