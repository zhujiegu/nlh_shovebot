"""
Evaluate hand
"""

from operator import mul
from functools import reduce
import itertools
import numpy as np

class HandScoreTables:
    """
    This class contain the two dictionary (one for flush and one for nonflush). 
    Keys are the product of prime numbers, values are the rank of the hand.

    Number of Distinct Hand Values:

    Straight Flush   10 
    Four of a Kind   156      [(13 choose 2) * (2 choose 1)]
    Full Houses      156      [(13 choose 2) * (2 choose 1)]
    Flush            1277     [(13 choose 5) - 10 straight flushes]
    Straight         10 
    Three of a Kind  858      [(13 choose 3) * (3 choose 1)]
    Two Pair         858      [(13 choose 3) * (3 choose 2)]
    One Pair         2860     [(13 choose 4) * (4 choose 1)]
    High Card      + 1277     [(13 choose 5) - 10 straights]
    -------------------------
    TOTAL            7462

    Assign prime to each card value for evaluating combination
    Name	2   3   4   5   6   7   8   9   T   J   Q   K   A
    Prime	2	3	5	7	11	13	17	19	23	29	31	37	41
    """

    # Largest rank of each kind
    MAX_STRAIGHT_FLUSH  = 10
    MAX_FOUR_OF_A_KIND  = 166
    MAX_FULL_HOUSE      = 322 
    MAX_FLUSH           = 1599
    MAX_STRAIGHT        = 1609
    MAX_THREE_OF_A_KIND = 2467
    MAX_TWO_PAIR        = 3325
    MAX_PAIR            = 6185
    MAX_HIGH_CARD       = 7462

    def __init__(self):
        # Create dictionary flush and non-flush
        self.flush_table = {}  
        self.nonflush_table = {}
        # value to prime
        self.prime_table = {
                1:41,
                2:2,
                3:3,
                4:5,
                5:7,
                6:11,
                7:13,
                8:17,
                9:19,
                10:23,
                11:29,
                12:31,
                13:37,
                14:41
        }
        # prime product of non-paired combinations (Cover flush table and part of non-flush table)
        # Including two lists: straight and high_card
        self.straight = []
        self.high_card = []
        self.non_pair()

        # Fill the table by calling the methods
        self.flush_write_table()
        self.nonflush_write_table()

    def flush_write_table(self):
        rank = 1
        for product in self.straight:
            self.flush_table[product] = rank
            rank +=1
        
        rank = HandScoreTables.MAX_FULL_HOUSE + 1
        for product in self.high_card:
            self.flush_table[product] = rank
            rank +=1

    def nonflush_write_table(self):
        prime = self.prime_table
        # Four of a kind:
        rank = HandScoreTables.MAX_STRAIGHT_FLUSH + 1
        for four in range(14,1,-1):
            for k in [i for i in range(14,1,-1) if i != four]:
                product = prime[four]**4 * prime[k] 
                self.nonflush_table[product] = rank
                rank +=1

        # Full house:
        rank = HandScoreTables.MAX_FOUR_OF_A_KIND + 1
        for three in range(14,1,-1):
            for pair in [i for i in range(14,1,-1) if i != three]:
                product = prime[three]**3 * prime[pair]**2 
                self.nonflush_table[product] = rank
                rank +=1

        # Straight
        rank = HandScoreTables.MAX_FLUSH + 1
        for product in self.straight:
            self.nonflush_table[product] = rank
            rank +=1

        # Three of a kind
        rank = HandScoreTables.MAX_STRAIGHT + 1
        for three in range(14,1,-1):
            for k1 in [i for i in range(14,2,-1) if i != three]:
                for k2 in [i for i in range(k1-1,1,-1) if i != three]:
                    product = prime[three]**3 * prime[k1] * prime[k2]
                    self.nonflush_table[product] = rank
                    rank +=1

        # Two pairs
        rank = HandScoreTables.MAX_THREE_OF_A_KIND + 1
        for p1 in range(14,2,-1):
            for p2 in range(p1-1,1,-1):
                for k in [i for i in range(14,1,-1) if i not in [p1,p2]]:
                    product = prime[p1]**2 * prime[p2]**2 * prime[k]
                    self.nonflush_table[product] = rank
                    rank +=1

        # One pair
        rank = HandScoreTables.MAX_TWO_PAIR + 1
        for pair in range(14,1,-1):
            for k1 in [i for i in range(14,3,-1) if i != pair]:
                for k2 in [i for i in range(k1-1,2,-1) if i != pair]:
                    for k3 in [i for i in range(k2-1,1,-1) if i != pair]:
                        product = prime[pair]**2 * prime[k1] * prime[k2] * prime[k3]
                        self.nonflush_table[product] = rank
                        rank +=1

        # High card
        rank = HandScoreTables.MAX_PAIR + 1
        for product in self.high_card:
            self.nonflush_table[product] = rank
            rank +=1

    def non_pair(self):
        # Straight prime product
        prime_table = self.prime_table
        for top in range(14,4,-1):
            product = prime_table[top] * prime_table[top-1] * prime_table[top-2] * prime_table[top-3] * prime_table[top-4]
            self.straight.append(product)

        # High card prime product
        for h1 in range(14,6,-1):
            for h2 in range(h1-1,4,-1):
                for h3 in range(h2-1,3,-1):
                    for h4 in range(h3-1,2,-1):
                        for h5 in range(h4-1,1,-1):
                            product = prime_table[h1] * prime_table[h2] * prime_table[h3] * \
                            prime_table[h4] * prime_table[h5]
                            self.high_card.append(product)
                            self.high_card = [i for i in self.high_card if i not in self.straight]


class Hand_Scorer:
    def __init__(self):
        # Create the dictionary, we need the flush_table, nonflush_table and "value to prime" table
        table = HandScoreTables()
        self.flush_table = table.flush_table
        self.nonflush_table = table.nonflush_table
        self.prime_table = table.prime_table
        # Determine which method to use based on the 5 out of [5,6,7] cards
        self.hand_size_map = {
            5 : self.five_cards,
            6 : self.six_cards,
            7 : self.seven_cards
        }

    def get_rank(self, playercards, board):
        """
        This is the function that the user calls to get a hand rank. 
        """
        all_cards = playercards + board
        return self.hand_size_map[len(all_cards)](all_cards)

    def winner(self, players, board):
        """
        Senario where players have different stacks in the poll is not considered, yet
        """
        ranks = []
        for holecards in [player.cards for player in players]:
            ranks.append(self.get_rank(holecards, board))
        win_hand = min(ranks)
        which_ones = [i for i, x in enumerate(ranks) if x == win_hand]
        players = np.array([player for player in players])
        return list(players[which_ones])


    def five_cards(self, cards):
        prime_product = reduce(mul, [self.prime_table[card.value] for card in cards], 1)
        if len( set(card.suit for card in cards) ) == 1:
            return self.flush_table[prime_product]
        else:
            return self.nonflush_table[prime_product]

    def six_cards(self, cards):
        minimum = HandScoreTables.MAX_HIGH_CARD

        all5cardcombobs = itertools.combinations(cards, 5)
        for combo in all5cardcombobs:
            score = self.five_cards(combo)
            if score < minimum:
                minimum = score
        return minimum

    def seven_cards(self, cards):
        minimum = HandScoreTables.MAX_HIGH_CARD

        all5cardcombobs = itertools.combinations(cards, 5)
        for combo in all5cardcombobs:
            score = self.five_cards(combo)
            if score < minimum:
                minimum = score
        return minimum