class Player:
    def __init__(self, name, buy_in, human = False):
        self.cards = []
        self.name = name
        self.buy_in = buy_in # initial stack
        self.stack = buy_in # updated stack
        self.bet = 0 # a single bet
        self.all_in = False
        self.fold = False
        self.position = "BT"
        self.ask_once = False # be asked at least once in a street
        self.human = human

    def addCard(self, card):
        self.cards.append(card)

    def cash_flow(self):
        pass

    def is_active(self):
        pass

    def reset(self):
        """
        reset per round
        """
        self.cards = []
        self.bet = 0 # a single bet
        self.street_bet = 0 # total bet in a street
        self.all_in = False
        self.fold = False
        self.ask_once = False


    def __repr__(self):
        return self.name
